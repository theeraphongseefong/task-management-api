package model

import (
	"github.com/golang-jwt/jwt/v5"
)

type MessageReturn struct {
	Status   string      `json:"status"`
	Msg      string      `json:"msg"`
	Response interface{} `json:"response"`
}

type ResponseModel struct {
	Status   string   `json:"status"`
	Msg      string   `json:"msg"`
	Response Response `json:"response"`
}

type Response struct {
	Data       interface{} `json:"data"`
	Pagination interface{} `json:"pagination,omitempty"`
}

type JwtClaims struct {
	Name     string `json:"name"`
	Username string `json:"username"`
	jwt.RegisteredClaims
}
