package model

type Task struct {
	ID          uint64
	UserID      uint64
	Title       string
	Description string
	DueDate     string
	Status      string
	CreatedAt   string
	UpdatedAt   string
}

type TaskAddRequest struct {
	Title       string `json:"title" validate:"required"`
	Description string `json:"description" validate:"required"`
	DueDate     string `json:"due_date" validate:"required"`
	Status      string `json:"status" validate:"required,oneof=pending in_progress completed"`
}

type TaskDetailResponse struct {
	ID          uint64 `json:"id" validate:"required"`
	Title       string `json:"title"`
	Description string `json:"description"`
	DueDate     string `json:"due_date"`
	Status      string `json:"status"`
	CreatedAt   string `json:"created_at"`
	UpdatedAt   string `json:"updated_at"`
}

type TaskListResponse struct {
	ID          uint64 `json:"id"`
	Title       string `json:"title"`
	Description string `json:"description"`
	DueDate     string `json:"due_date"`
	Status      string `json:"status"`
}

type TaskUpdateRequest struct {
	ID          uint64 `json:"id" validate:"required,min=1"`
	Title       string `json:"title" validate:"required"`
	Description string `json:"description" validate:"required"`
	DueDate     string `json:"due_date" validate:"required"`
	Status      string `json:"status" validate:"required,oneof=pending in_progress completed"`
}
