package model

import (
	"github.com/golang-jwt/jwt/v5"
)

type User struct {
	ID        uint64
	Username  string
	Password  string
	Name      string
	CreatedAt string
	UpdatedAt string
}

type LoginRequest struct {
	Username string `json:"username" validate:"required,max=16,alphanum"`
	Password string `json:"password" validate:"required,max=50"`
}

type RegisterUserRequest struct {
	Username string `json:"username" validate:"required,max=16,alphanum"`
	Password string `json:"password" validate:"required,max=50"`
	Name     string `json:"name" validate:"required,max=50"`
}

type JWTResponse struct {
	Token string `json:"token"`
}

type JwtCustomClaims struct {
	UUID string `json:"uuid"`
	Name string `json:"name"`
	jwt.RegisteredClaims
}
