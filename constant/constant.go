package constant

// Request Constant
const (
	StatusOK      = "OK"
	StatusSuccess = "success"
	StatusError   = "error"
)

// Format Date
const (
	FORMAT_DATE_TIME string = "2006-01-02 15:04:05"
)

// Error
const (
	ErrorLogin     string = "username or password not match!"
	ErrorNotUpdate string = "not update"
)
