package utility

import (
	"fmt"

	"golang.org/x/crypto/bcrypt"
)

// HashPassword -> stringHash
func HashPassword(password string, salt string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(fmt.Sprintf(`%s%s`, password, salt)), bcrypt.DefaultCost)
	return string(bytes), err
}

// CheckPasswordHash -> bool
func CheckPasswordHash(password string, salt string, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(fmt.Sprintf(`%s%s`, password, salt)))
	if err != nil {
		return false
	}
	return err == nil
}
