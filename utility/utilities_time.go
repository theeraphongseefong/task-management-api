package utility

import (
	"task-management-api/constant"
	"time"
)

// GetTime -> String Time Format
func GetTimeZone(timeZone string) (string, error) {
	loc, err := time.LoadLocation(timeZone)
	if err != nil {
		return "", err
	}

	return time.Now().In(loc).Format(constant.FORMAT_DATE_TIME), err
}
