package utility

import (
	"task-management-api/model"
	"time"

	"github.com/golang-jwt/jwt/v5"
	"github.com/labstack/echo/v4"
)

// Create JWT
func CreateJWT(username string, name string, jwtExp int, jwtSecret string) (string, error) {
	// Set custom claims
	claims := &model.JwtClaims{
		Name:             name,
		Username:         username,
		RegisteredClaims: jwt.RegisteredClaims{ExpiresAt: jwt.NewNumericDate(time.Now().Add(time.Hour * time.Duration(jwtExp)))},
	}

	// Create token with claims
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	// Generate encoded token and send it as response.
	jwtToken, err := token.SignedString([]byte(jwtSecret))
	if err != nil {
		LogError.Println("Error Signed JWT String:", err)
		return "", err
	}
	return jwtToken, nil
}

func DecodeJWT(c echo.Context) (*model.JwtClaims, error) {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(*model.JwtClaims)

	response := model.JwtClaims{
		Name:             claims.Name,
		Username:         claims.Username,
		RegisteredClaims: jwt.RegisteredClaims{},
	}
	return &response, nil
}
