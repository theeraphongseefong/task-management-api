package routes

import (
	"net/http"
	"task-management-api/constant"
	"task-management-api/model"

	"github.com/labstack/echo/v4"
)

func (h Handlers) RegisterUser(c echo.Context) error {
	//State: Initial RegisterUserRequest
	request := model.RegisterUserRequest{}

	//State: Initial MessageReturn
	response := model.MessageReturn{}

	//State: Bind Data From Frontend
	if err := c.Bind(&request); err != nil {
		response.Status = constant.StatusError
		response.Msg = err.Error()
		return c.JSON(http.StatusBadRequest, response)
	}

	//State: Validate request
	if err := c.Validate(request); err != nil {
		response.Status = constant.StatusError
		response.Msg = err.Error()
		return c.JSON(http.StatusBadRequest, response)
	}

	//State: Register
	loginToken, err := h.usecases.UCRegisterUser(request)
	if err != nil {
		response.Status = constant.StatusError
		response.Msg = err.Error()
		return c.JSON(http.StatusBadRequest, response)
	}
	response.Status = constant.StatusSuccess
	response.Response = loginToken
	return c.JSON(http.StatusOK, response)
}

func (h Handlers) Login(c echo.Context) error {
	//State: Initial LoginRequest
	request := model.LoginRequest{}

	//State: Initial MessageReturn
	response := model.MessageReturn{}

	//State: Bind Data From Frontend
	if err := c.Bind(&request); err != nil {
		response.Status = constant.StatusError
		response.Msg = err.Error()
		return c.JSON(http.StatusBadRequest, response)
	}

	//State: Validate request
	if err := c.Validate(request); err != nil {
		response.Status = constant.StatusError
		response.Msg = err.Error()
		return c.JSON(http.StatusBadRequest, response)
	}

	//State: loginToken
	loginToken, err := h.usecases.UCLogin(request)
	if err != nil {
		response.Status = constant.StatusError
		response.Msg = err.Error()
		return c.JSON(http.StatusBadRequest, response)
	}
	response.Status = constant.StatusSuccess
	response.Response = loginToken
	return c.JSON(http.StatusOK, response)
}
