package routes

import (
	"fmt"
	"net/http"
	"task-management-api/constant"
	"task-management-api/environment"
	"task-management-api/model"
	"task-management-api/usecases"

	"github.com/labstack/echo/v4"
)

type Handlers struct {
	usecases usecases.UCApiInterface
	env      *environment.Environment
}

func NewHandlers(env *environment.Environment, usecases usecases.UCApiInterface) Handlers {
	return Handlers{
		usecases: usecases,
		env:      env,
	}
}

func customHTTPErrorHandler(err error, c echo.Context) {
	code := 0
	message := ""
	if he, ok := err.(*echo.HTTPError); ok {
		code = he.Code
		message = fmt.Sprintf("%v", he.Message)
	}
	response := &model.MessageReturn{
		Status:   constant.StatusError,
		Msg:      message,
		Response: nil,
	}
	c.JSON(code, response)
}

func (h Handlers) HealthCheck(c echo.Context) error {
	return c.String(http.StatusOK, constant.StatusOK)
}
