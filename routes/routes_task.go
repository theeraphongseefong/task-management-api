package routes

import (
	"net/http"
	"strconv"
	"task-management-api/constant"
	"task-management-api/model"

	"github.com/labstack/echo/v4"
)

func (h Handlers) TaskAdd(c echo.Context) error {
	//State: Initial TaskAddRequest
	request := model.TaskAddRequest{}

	//State: Initial MessageReturn
	response := model.MessageReturn{}

	//State: Bind Data From Frontend
	if err := c.Bind(&request); err != nil {
		response.Status = constant.StatusError
		response.Msg = err.Error()
		return c.JSON(http.StatusBadRequest, response)
	}

	//State: Validate request
	if err := c.Validate(request); err != nil {
		response.Status = constant.StatusError
		response.Msg = err.Error()
		return c.JSON(http.StatusBadRequest, response)
	}

	//State: Task Add
	err := h.usecases.UCTaskAdd(c, request)
	if err != nil {
		response.Status = constant.StatusError
		response.Msg = err.Error()
		return c.JSON(http.StatusBadRequest, response)
	}
	response.Status = constant.StatusSuccess
	return c.JSON(http.StatusOK, response)
}

func (h Handlers) TaskDetailByID(c echo.Context) error {
	//State: Initial MessageReturn
	response := model.MessageReturn{}

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		response.Status = constant.StatusError
		response.Msg = err.Error()
		return c.JSON(http.StatusBadRequest, response)
	}

	//State: Detail By ID
	task, err := h.usecases.UCTaskDetailByID(c, uint64(id))
	if err != nil {
		response.Status = constant.StatusError
		response.Msg = err.Error()
		return c.JSON(http.StatusBadRequest, response)
	}
	response.Status = constant.StatusSuccess
	response.Response = task
	return c.JSON(http.StatusOK, response)
}

func (h Handlers) TaskList(c echo.Context) error {
	//State: Initial MessageReturn
	response := model.MessageReturn{}

	//State: List
	list, err := h.usecases.UCTaskList(c)
	if err != nil {
		response.Status = constant.StatusError
		response.Msg = err.Error()
		return c.JSON(http.StatusBadRequest, response)
	}
	response.Status = constant.StatusSuccess
	response.Response = list.Data
	return c.JSON(http.StatusOK, response)
}

func (h Handlers) TaskUpdate(c echo.Context) error {
	//State: Initial TaskUpdateRequest
	request := model.TaskUpdateRequest{}

	//State: Initial MessageReturn
	response := model.MessageReturn{}

	//State: Bind Data From Frontend
	if err := c.Bind(&request); err != nil {
		response.Status = constant.StatusError
		response.Msg = err.Error()
		return c.JSON(http.StatusBadRequest, response)
	}

	//State: Validate request
	if err := c.Validate(request); err != nil {
		response.Status = constant.StatusError
		response.Msg = err.Error()
		return c.JSON(http.StatusBadRequest, response)
	}

	//State: Update
	err := h.usecases.UCTaskUpdate(c, request)
	if err != nil {
		response.Status = constant.StatusError
		response.Msg = err.Error()
		return c.JSON(http.StatusBadRequest, response)
	}
	response.Status = constant.StatusSuccess
	return c.JSON(http.StatusOK, response)
}

func (h Handlers) TaskDelete(c echo.Context) error {
	//State: Initial MessageReturn
	response := model.MessageReturn{}

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		response.Status = constant.StatusError
		response.Msg = err.Error()
		return c.JSON(http.StatusBadRequest, response)
	}

	//State: Register
	err = h.usecases.UCTaskDelete(c, uint64(id))
	if err != nil {
		response.Status = constant.StatusError
		response.Msg = err.Error()
		return c.JSON(http.StatusBadRequest, response)
	}
	response.Status = constant.StatusSuccess
	return c.JSON(http.StatusOK, response)
}
