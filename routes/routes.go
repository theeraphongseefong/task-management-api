package routes

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"task-management-api/environment"
	"task-management-api/model"
	"task-management-api/usecases"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/golang-jwt/jwt/v5"
	echojwt "github.com/labstack/echo-jwt/v4"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

type CustomValidator struct {
	validator *validator.Validate
}

func (cv *CustomValidator) Validate(i interface{}) error {
	if err := cv.validator.Struct(i); err != nil {
		// Optionally, you could return the error to give each route more control over the status code
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	return nil
}

func Init(env *environment.Environment, usecases usecases.UCApiInterface) {
	// Initial echo_api api server
	e := echo.New()
	e.Use(middleware.Recover())
	e.HTTPErrorHandler = customHTTPErrorHandler
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{http.MethodGet, http.MethodHead, http.MethodPost},
	}))

	e.Validator = &CustomValidator{validator: validator.New()}

	// Set Time Out
	e.Use(middleware.TimeoutWithConfig(middleware.TimeoutConfig{
		ErrorMessage: `{
	"status": "error",
	"msg": "timeout",
	"response": null
}`,
		Timeout: 20 * time.Second,
	}))

	Handler := NewHandlers(env, usecases)

	// api
	auth := e.Group("/v1/auth")
	auth.POST("/register", Handler.RegisterUser)
	auth.POST("/login", Handler.Login)

	configJWT := echojwt.Config{
		NewClaimsFunc: func(c echo.Context) jwt.Claims {
			return new(model.JwtClaims)
		},
		SigningKey: []byte(env.JWT_SECRET_KEY),
	}

	// api
	api := e.Group("/v1/api")
	api.Use(echojwt.WithConfig(configJWT))

	apiTask := api.Group("/task")
	apiTask.POST("/add", Handler.TaskAdd)
	apiTask.GET("/list", Handler.TaskList)
	apiTask.GET("/detail/:id", Handler.TaskDetailByID)
	apiTask.PATCH("/update", Handler.TaskUpdate)
	apiTask.DELETE("/delete/:id", Handler.TaskDelete)

	// Start server
	go func() {
		if err := e.Start(fmt.Sprintf(":%s", env.PORT)); err != nil && err != http.ErrServerClosed {
			e.Logger.Fatal("shutting down the server")
		}
	}()

	// Wait for interrupt signal to gracefully shutdown the server with a timeout of 10 seconds.
	// Use a buffered channel to avoid missing signals as recommended for signal.Notify
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if err := e.Shutdown(ctx); err != nil {
		e.Logger.Fatal(err)
	}
}
