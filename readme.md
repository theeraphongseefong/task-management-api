# task-management-api
task-management-api build with Golang

## Required Software
- [docker desktop](https://www.docker.com/products/docker-desktop/)
- [postman](https://www.postman.com/downloads/)


## Build With
* Echo - Go web framework
* Postgres - Database

## How To Run
- 1. Create .env file project work space
```
# Echo
PORT=8080

# Database
HOST_DB=192.168.1.100 #Change This To your IP 
DATABASE_DB="postgres"
USERNAME_DB="postgres"
PASSWORD_DB="password"
PORT_DB=5432
TIME_LOC_DB="Asia/Bangkok"
SSL_MODE=disable

# Time
LOC_TIMEZONE="Asia/Bangkok"

# Salt Password
SALT_PASSWORD="password"

# JWT
JWT_EXPIRE_TIME=60
JWT_SECRET_KEY="secret"
```
- 2. run this command in terminal
```
docker-compose up --build
```

- 3. go to postman 
```
#Import File
- Local.postman_environment.json
- API.postman_collection.json
```
- 4. play with postman



## Authors
- [theeraphongseefong@gmail.com](https://gitlab.com/theeraphongseefong/)