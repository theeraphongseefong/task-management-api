package usecases

import (
	"task-management-api/model"
	"task-management-api/utility"

	"github.com/labstack/echo/v4"
)

/*
	WorkFlow:
		- 1. Decode JWT
		- 2. Call User By Username
		- 3. Set Parameter
		- 4. Create Task
*/
// UCTaskAdd implements UCApiInterface.
func (usecases ApiUc) UCTaskAdd(c echo.Context, req model.TaskAddRequest) (err error) {
	//State: Decode JWT
	jwt, err := utility.DecodeJWT(c)
	if err != nil {
		utility.LogError.Println(err)
		return err
	}

	//State: Call User By Username
	user, err := usecases.repoDatabase.RPCallUserByUsername(jwt.Username)
	if err != nil {
		utility.LogError.Println(err)
		return err
	}

	//State: Set Parameter
	timeNow, err := utility.GetTimeZone(usecases.env.LOC_TIMEZONE)
	if err != nil {
		utility.LogError.Println(err)
		return err
	}

	//State: Create Task
	newUser := model.Task{
		UserID:      user.ID,
		Title:       req.Title,
		Description: req.Description,
		DueDate:     req.DueDate,
		Status:      req.Status,
		CreatedAt:   timeNow,
		UpdatedAt:   timeNow,
	}

	err = usecases.repoDatabase.RPTaskAdd(newUser)
	if err != nil {
		return err
	}

	return nil
}

/*
	WorkFlow:
		- 1. Decode JWT
		- 2. Call User By Username
		- 3. Detail
		- 4. Return Response
*/
// UCTaskDetailByID implements UCApiInterface.
func (usecases ApiUc) UCTaskDetailByID(c echo.Context, id uint64) (response *model.TaskDetailResponse, err error) {
	//State: Decode JWT
	jwt, err := utility.DecodeJWT(c)
	if err != nil {
		utility.LogError.Println(err)
		return nil, err
	}

	//State: Call User By Username
	user, err := usecases.repoDatabase.RPCallUserByUsername(jwt.Username)
	if err != nil {
		utility.LogError.Println(err)
		return nil, err
	}

	//State: Detail Task
	task, err := usecases.repoDatabase.RPTaskDetailByID(id, user.ID)
	if err != nil {
		return nil, err
	}

	//State: Return Response
	response = &model.TaskDetailResponse{
		ID:          task.ID,
		Title:       task.Title,
		Description: task.Description,
		DueDate:     task.DueDate,
		Status:      task.Status,
		CreatedAt:   task.CreatedAt,
		UpdatedAt:   task.UpdatedAt,
	}
	return response, err
}

/*
	WorkFlow:
		- 1. Decode JWT
		- 2. Call User By Username
		- 3. Task List
		- 4. Return Response
*/
// UCTaskList implements UCApiInterface.
func (usecases ApiUc) UCTaskList(c echo.Context) (response *model.Response, err error) {
	//State: Decode JWT
	jwt, err := utility.DecodeJWT(c)
	if err != nil {
		utility.LogError.Println(err)
		return response, err
	}

	//State: Call User By Username
	user, err := usecases.repoDatabase.RPCallUserByUsername(jwt.Username)
	if err != nil {
		utility.LogError.Println(err)
		return response, err
	}

	//State: Task List
	task, err := usecases.repoDatabase.RPTaskList(user.ID)
	if err != nil {
		return response, err
	}

	taskList := []model.TaskListResponse{}
	for _, data := range task {
		taskList = append(taskList, model.TaskListResponse{
			ID:          data.ID,
			Title:       data.Title,
			Description: data.Description,
			DueDate:     data.DueDate,
			Status:      data.Status,
		})
	}

	//State: Return Response
	response = &model.Response{
		Data: taskList,
	}

	return response, nil
}

/*
	WorkFlow:
		- 1. Decode JWT
		- 2. Call User By Username
		- 3. Delete Task
*/
// UCTaskDelete implements UCApiInterface.
func (usecases ApiUc) UCTaskDelete(c echo.Context, id uint64) (err error) {
	//State: Decode JWT
	jwt, err := utility.DecodeJWT(c)
	if err != nil {
		utility.LogError.Println(err)
		return err
	}

	//State: Call User By Username
	user, err := usecases.repoDatabase.RPCallUserByUsername(jwt.Username)
	if err != nil {
		utility.LogError.Println(err)
		return err
	}

	//State: Delete Task
	err = usecases.repoDatabase.RPTaskDelete(id, user.ID)
	if err != nil {
		return err
	}
	return nil
}

/*
	WorkFlow:
		- 1. Decode JWT
		- 2. Call User By Username
		- 3. Set Parameter
		- 4. Update Task
*/
// UCTaskUpdate implements UCApiInterface.
func (usecases ApiUc) UCTaskUpdate(c echo.Context, req model.TaskUpdateRequest) (err error) {
	//State: Decode JWT
	jwt, err := utility.DecodeJWT(c)
	if err != nil {
		utility.LogError.Println(err)
		return err
	}

	//State: Call User By Username
	user, err := usecases.repoDatabase.RPCallUserByUsername(jwt.Username)
	if err != nil {
		utility.LogError.Println(err)
		return err
	}

	//State: Set Parameter
	timeNow, err := utility.GetTimeZone(usecases.env.LOC_TIMEZONE)
	if err != nil {
		utility.LogError.Println(err)
		return err
	}

	//State: Update Task
	task := model.Task{
		ID:          req.ID,
		UserID:      user.ID,
		Title:       req.Title,
		Description: req.Description,
		DueDate:     req.DueDate,
		Status:      req.Status,
		UpdatedAt:   timeNow,
	}

	err = usecases.repoDatabase.RPTaskUpdate(task)
	if err != nil {
		return err
	}

	return nil
}
