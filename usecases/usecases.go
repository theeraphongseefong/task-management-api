package usecases

import (
	"task-management-api/environment"
	"task-management-api/model"
	"task-management-api/repository/database"

	"github.com/labstack/echo/v4"
)

// UseCase API
type UCApiInterface interface {
	UCRegisterUser(req model.RegisterUserRequest) (response *model.Response, err error)
	UCLogin(req model.LoginRequest) (response *model.Response, err error)

	UCTaskAdd(c echo.Context, req model.TaskAddRequest) (err error)
	UCTaskList(c echo.Context) (response *model.Response, err error)
	UCTaskDetailByID(c echo.Context, id uint64) (response *model.TaskDetailResponse, err error)
	UCTaskUpdate(c echo.Context, req model.TaskUpdateRequest) (err error)
	UCTaskDelete(c echo.Context, id uint64) (err error)
}

type ApiUc struct {
	env          *environment.Environment
	repoDatabase database.RPDatabaseInterface
}

func ApiUseCase(env *environment.Environment, databaseInterface database.RPDatabaseInterface) UCApiInterface {
	return ApiUc{
		env:          env,
		repoDatabase: databaseInterface,
	}
}
