package usecases

import (
	"fmt"
	"task-management-api/constant"
	"task-management-api/model"

	"task-management-api/utility"
)

/*
	WorkFlow:
		- 1. Set Parameter
		- 2. Hash Password
		- 3. Create User
		- 4. Create JWT
		- 5. Return Response
*/
// RegisterUser implements UCApiInterface.
func (usecases ApiUc) UCRegisterUser(req model.RegisterUserRequest) (response *model.Response, err error) {
	//State: Set Parameter
	timeNow, err := utility.GetTimeZone(usecases.env.LOC_TIMEZONE)
	if err != nil {
		utility.LogError.Println(err)
		return nil, err
	}

	//State: Hash Password
	password, err := utility.HashPassword(req.Password, usecases.env.SALT_PASSWORD)
	if err != nil {
		utility.LogError.Println(err)
		return nil, err
	}

	//State: Create User
	newUser := model.User{
		Username:  req.Username,
		Password:  password,
		Name:      req.Name,
		CreatedAt: timeNow,
		UpdatedAt: timeNow,
	}
	err = usecases.repoDatabase.RPCreateUser(newUser)
	if err != nil {
		return nil, err
	}

	//State: Create JWT
	jwtToken, err := utility.CreateJWT(req.Username, req.Name, usecases.env.JWT_EXPIRE_TIME, usecases.env.JWT_SECRET_KEY)
	if err != nil {
		utility.LogError.Println(err)
		return nil, err
	}

	//State: Return Response
	response = &model.Response{
		Data: model.JWTResponse{
			Token: jwtToken,
		},
	}
	return response, nil
}

/*
	WorkFlow:
		- 1. Call User By Username
		- 2. Check Password
		- 3. Create JWT
		- 4. Return Response
*/
// UCLogin implements UCApiInterface.
func (usecases ApiUc) UCLogin(req model.LoginRequest) (response *model.Response, err error) {
	//State: Call User By Username
	user, err := usecases.repoDatabase.RPCallUserByUsername(req.Username)
	if err != nil {
		return nil, err
	}

	//State: Hash Password
	if !utility.CheckPasswordHash(req.Password, usecases.env.SALT_PASSWORD, user.Password) {
		utility.LogError.Println(constant.ErrorLogin)
		return nil, fmt.Errorf("%s", constant.ErrorLogin)
	}

	//State: Create JWT
	jwtToken, err := utility.CreateJWT(req.Username, user.Name, usecases.env.JWT_EXPIRE_TIME, usecases.env.JWT_SECRET_KEY)
	if err != nil {
		utility.LogError.Println(err)
		return nil, err
	}

	//State: Return Response
	response = &model.Response{
		Data: model.JWTResponse{
			Token: jwtToken,
		},
	}
	return response, nil
}
