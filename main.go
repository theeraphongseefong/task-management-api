package main

import (
	"os"
	"task-management-api/environment"
	"task-management-api/repository/database"
	"task-management-api/routes"
	"task-management-api/usecases"
	"task-management-api/utility"
)

func main() {
	//State: Load Environment
	env, err := environment.Load()
	if err != nil {
		utility.LogError.Fatalln(err)
	}

	//State: Set Timezone
	err = os.Setenv("TZ", env.LOC_TIMEZONE)
	if err != nil {
		utility.LogError.Fatalln(err)
	}

	// Create Repositories
	databaseRp, err := database.Connect(env)
	if err != nil {
		utility.LogError.Fatalln(err)
	}

	// Create usecases
	apiUseCase := usecases.ApiUseCase(env, databaseRp)
	routes.Init(env, apiUseCase)
}
