package database

import (
	"errors"
	"task-management-api/constant"
	"task-management-api/model"
	"task-management-api/repository/database/entity"
	"task-management-api/utility"
)

// RPTaskAdd implements RPDatabaseInterface.
func (database RPDatabase) RPTaskAdd(param model.Task) (err error) {
	err = database.database.Create(&entity.Task{
		UserID:      param.UserID,
		Title:       param.Title,
		Description: param.Description,
		DueDate:     param.DueDate,
		Status:      param.Status,
		CreatedAt:   param.CreatedAt,
		UpdatedAt:   param.UpdatedAt,
	}).Error
	if err != nil {
		utility.LogError.Println(err)
		return err
	}
	return nil
}

// RPTaskDelete implements RPDatabaseInterface.
func (database RPDatabase) RPTaskDelete(id uint64, userID uint64) (err error) {
	query := database.database.Where("id = ? AND user_id = ?", id, userID).Delete(&entity.Task{})
	if err != nil {
		utility.LogError.Println(err)
		return err
	}
	if query.Error != nil {
		return query.Error
	}
	if query.RowsAffected <= 0 {
		return errors.New(constant.ErrorNotUpdate)
	}

	return nil
}

// RPTaskDetailByID implements RPDatabaseInterface.
func (database RPDatabase) RPTaskDetailByID(id uint64, userID uint64) (response *model.Task, err error) {
	taskEntity := &entity.Task{}
	err = database.database.Where("id = ? AND user_id = ?", id, userID).Take(taskEntity).Error
	if err != nil {
		utility.LogError.Println(err)
		return nil, err
	}
	response = &model.Task{
		ID:          taskEntity.ID,
		UserID:      taskEntity.UserID,
		Title:       taskEntity.Title,
		Description: taskEntity.Description,
		DueDate:     taskEntity.DueDate,
		Status:      taskEntity.Status,
		CreatedAt:   taskEntity.CreatedAt,
		UpdatedAt:   taskEntity.UpdatedAt,
	}
	return response, nil
}

// RPTaskList implements RPDatabaseInterface.
func (database RPDatabase) RPTaskList(userID uint64) (response []model.Task, err error) {
	taskEntity := []entity.Task{}
	err = database.database.Where("user_id = ?", userID).Find(&taskEntity).Error
	if err != nil {
		utility.LogError.Println(err)
		return nil, err
	}

	for _, data := range taskEntity {
		response = append(response, model.Task{
			ID:          data.ID,
			UserID:      data.UserID,
			Title:       data.Title,
			Description: data.Description,
			DueDate:     data.DueDate,
			Status:      data.Status,
			CreatedAt:   data.CreatedAt,
			UpdatedAt:   data.UpdatedAt,
		})
	}
	return response, nil
}

// RPTaskUpdate implements RPDatabaseInterface.
func (database RPDatabase) RPTaskUpdate(param model.Task) (err error) {
	query := database.database.Model(&model.Task{}).Where("id = ? AND user_id = ?", param.ID, param.UserID).Updates(entity.Task{
		Title:       param.Title,
		Description: param.Description,
		DueDate:     param.DueDate,
		Status:      param.Status,
		UpdatedAt:   param.UpdatedAt,
	})
	if query.Error != nil {
		return err
	}
	if query.RowsAffected <= 0 {
		return errors.New(constant.ErrorNotUpdate)
	}

	return nil
}
