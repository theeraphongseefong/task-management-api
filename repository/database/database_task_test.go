package database

import (
	"fmt"
	"task-management-api/model"
	"testing"
	"time"

	// "task-management-api/repository/database"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func TestTaskAdd(t *testing.T) {
	// create mock connection and mock DB
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	gormDB, err := gorm.Open(postgres.New(postgres.Config{
		Conn: db,
	}), &gorm.Config{})
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a gorm database connection", err)
	}
	// defer gormDB.Close()

	database := RPDatabase{
		database: gormDB,
	}

	// create instance struct Task for insert
	now := time.Now().Format(time.RFC3339)
	tasks := []model.Task{
		{
			UserID:      1,
			Title:       "Task Title",
			Description: "Task Description",
			DueDate:     now,
			Status:      "Pending",
			CreatedAt:   now,
			UpdatedAt:   now,
		},
		{
			UserID:      2,
			Title:       "Task Title2",
			Description: "Task Description2",
			DueDate:     now,
			Status:      "Pending",
			CreatedAt:   now,
			UpdatedAt:   now,
		},
	}

	expected := []error{nil, nil}

	// Expect
	for i, task := range tasks {
		mock.ExpectExec("INSERT INTO tasks").WillReturnError(expected[i])
		database.RPTaskAdd(task)
		assert.NoError(t, err)
	}
}

func TestTaskUpdate(t *testing.T) {
	// create mock connection and mock DB
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	gormDB, err := gorm.Open(postgres.New(postgres.Config{
		Conn: db,
	}), &gorm.Config{})
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a gorm database connection", err)
	}
	// defer gormDB.Close()

	database := RPDatabase{
		database: gormDB,
	}

	// create instance struct Task for update
	now := time.Now().Format(time.RFC3339)
	tasks := []model.Task{
		{
			ID:          1,
			UserID:      1,
			Title:       "Task Title",
			Description: "Task Description",
			DueDate:     now,
			Status:      "Pending",
			CreatedAt:   now,
			UpdatedAt:   now,
		},
	}

	expected := []error{nil}

	// Expect
	for i, task := range tasks {
		mock.ExpectExec("UPDATE tasks").WillReturnError(expected[i])
		database.RPTaskUpdate(task)
		assert.NoError(t, err)
	}
}

func TestTaskDelete(t *testing.T) {
	// create mock connection and mock DB
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	gormDB, err := gorm.Open(postgres.New(postgres.Config{
		Conn: db,
	}), &gorm.Config{})
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a gorm database connection", err)
	}
	// defer gormDB.Close()

	database := RPDatabase{
		database: gormDB,
	}

	// Expect
	mock.ExpectExec("DELETE tasks").WillReturnError(nil)
	database.RPTaskDelete(1, 2)
	assert.NoError(t, err)
}

func TestTaskDetailByID(t *testing.T) {
	// create mock connection and mock DB
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	gormDB, err := gorm.Open(postgres.New(postgres.Config{
		Conn: db,
	}), &gorm.Config{})
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a gorm database connection", err)
	}
	// defer gormDB.Close()

	database := RPDatabase{
		database: gormDB,
	}

	// create instance Task
	now := time.Now().Format(time.RFC3339)
	tasks := model.Task{
		ID:          1,
		UserID:      1,
		Title:       "Task Title",
		Description: "Task Description",
		DueDate:     now,
		Status:      "Pending",
		CreatedAt:   now,
		UpdatedAt:   now,
	}
	// Expect
	rows := sqlmock.NewRows([]string{"id", "user_id", "title", "description"}).
		AddRow(tasks.ID, tasks.UserID, tasks.Title, tasks.Description)

	mock.ExpectQuery(fmt.Sprintf("SELECT * FROM tasks WHERE id = %d AND user_id = %d", 1, 1)).WillReturnRows(rows)
	database.RPTaskDetailByID(1, 1)
	assert.NoError(t, err)
}
