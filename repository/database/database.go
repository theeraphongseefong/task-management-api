package database

import (
	"fmt"
	"task-management-api/environment"
	"task-management-api/model"
	"task-management-api/repository/database/entity"
	"task-management-api/utility"
	"time"

	"gorm.io/driver/postgres"

	"gorm.io/gorm"
)

// Repository Database
type RPDatabaseInterface interface {
	// User
	RPCreateUser(param model.User) (err error)
	RPCallUserByUsername(param string) (response *model.User, err error)

	// Task
	RPTaskAdd(param model.Task) (err error)
	RPTaskList(userID uint64) (response []model.Task, err error)
	RPTaskDetailByID(id uint64, userID uint64) (response *model.Task, err error)
	RPTaskUpdate(param model.Task) (err error)
	RPTaskDelete(id uint64, userID uint64) (err error)
}

type RPDatabase struct {
	database *gorm.DB
}

const maxIdleConns = 5
const maxOpenConns = 10

func Connect(env *environment.Environment) (RPDatabaseInterface, error) {
	//State: Sprintf string
	databaseDsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=%s TimeZone=%s",
		env.HOST_DB, env.USERNAME_DB, env.PASSWORD_DB, env.DATABASE_DB, env.PORT_DB, env.SSL_MODE, env.LOC_TIMEZONE)

	//State: Connect Database
	dbConnection, err := gorm.Open(postgres.Open(databaseDsn), &gorm.Config{})
	if err != nil {
		utility.LogError.Println(err)
		return nil, err
	}

	//State: Set Option Connection
	db, err := dbConnection.DB()
	if err != nil {
		utility.LogError.Println(err)
		defer db.Close()
		return nil, err
	}
	if err = db.Ping(); err != nil {
		utility.LogError.Println(err)
		defer db.Close()
		return nil, err
	}
	db.SetMaxIdleConns(maxIdleConns)
	db.SetMaxOpenConns(maxOpenConns)
	db.SetConnMaxLifetime(time.Hour * 1)

	dbConnection.AutoMigrate(&entity.Task{}, entity.User{})

	return RPDatabase{
		database: dbConnection,
	}, err
}
