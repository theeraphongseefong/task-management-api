package database

import (
	"task-management-api/model"
	"task-management-api/repository/database/entity"
	"task-management-api/utility"
)

// RPCreateUser implements RPDatabaseInterface.
func (database RPDatabase) RPCreateUser(param model.User) (err error) {
	err = database.database.Create(&entity.User{
		Username:  param.Username,
		Password:  param.Password,
		Name:      param.Name,
		CreatedAt: param.CreatedAt,
		UpdatedAt: param.UpdatedAt,
	}).Error
	if err != nil {
		utility.LogError.Println(err)
		return err
	}
	return nil
}

// RPCallUserByUsername implements RPDatabaseInterface.
func (database RPDatabase) RPCallUserByUsername(param string) (response *model.User, err error) {
	entityModel := &entity.User{}
	err = database.database.Take(&entityModel).Error
	if err != nil {
		utility.LogError.Println(err)
		return nil, err
	}

	response = &model.User{
		ID:        entityModel.ID,
		Username:  entityModel.Username,
		Password:  entityModel.Password,
		Name:      entityModel.Name,
		CreatedAt: entityModel.CreatedAt,
		UpdatedAt: entityModel.UpdatedAt,
	}
	return response, nil
}
