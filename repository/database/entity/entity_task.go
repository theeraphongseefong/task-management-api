package entity

import "gorm.io/gorm"

type Task struct {
	ID          uint64
	UserID      uint64
	Title       string
	Description string
	DueDate     string
	Status      string
	CreatedAt   string
	UpdatedAt   string
	DeletedAt   gorm.DeletedAt `gorm:"index"`
}
