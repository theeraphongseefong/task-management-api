package entity

import (
	"gorm.io/gorm"
)

type User struct {
	ID        uint64
	Username  string `gorm:"index:idx_username,unique"`
	Password  string
	Name      string
	CreatedAt string
	UpdatedAt string
	DeletedAt gorm.DeletedAt `gorm:"index"`
}
