FROM golang:alpine3.18

LABEL maintainer="Tommy"

WORKDIR /app
COPY go.mod go.sum ./

RUN go mod download

COPY . .

EXPOSE 8080

RUN go build -o /dist/app .

CMD ["sh", "-c", "/dist/app"]

